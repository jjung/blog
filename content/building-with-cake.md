---
title: "Building .NET applications using the CLI"
date: Sun Apr 22 14:36:57 2018
draft: false
description: "An introduction to Cake"
aliases: 
  - "/posts/building-with-cake/"
---

Visual Studio allows you to develop, test, and compile .NET Framework applications inside a single IDE.

However, if you want to set up automated builds for a Continuous Integration pipeline you'll need to learn how to do so via the command line.

In this post, we'll set up a build server to compile .NET Framework applications using [Cake](https://cakebuild.net/).

## When is this useful?

Creating a continuous integration pipeline will allow you to automatically:

1. Compile your application
2. Run tests
3. Create binaries / installers
4. Deploy your application

## Is this just for Windows?

If you're writing a .NET Core application and want to build on Linux, the process is a little different.  For simplicity's sake, we'll only cover Windows in this post.

## Prerequisites

* Windows Server 2012 R2 or higher

### Installing Build Tools
1. Download [Build Tools for Visual Studio 2019](https://visualstudio.microsoft.com/thank-you-downloading-visual-studio/?sku=BuildTools&rel=16)
2. Click continue
3. In the **Workloads** section, choose the types of applications you will be building.  
    - I chose **Web Development Build Tools** and **.NET Core Build Tools**
4. In the **Individual Components** section, choose the targeting pack and SDK for any of the .NET Framework versions your applications use.
5. Check **NuGet targets and build tasks**
6. Click install
7. Go take a break!  This will take ~5-10 minutes.
8. Click restart when it's all done

### Using Cake to build an application

[Cake](https://cakebuild.net/) is a tool that allows you to write C# in order to build, test, and copy files.  We'll be using it to build a [sample project](https://cakebuild.net/docs/tutorials/getting-started) provided by Cake.

Download the [sample project zip](https://github.com/cake-build/example/archive/master.zip) and extract it.

Here are the two files that are used to perform the build:

* **build.ps1**
  * Installs cake if it is not found on your computer
* **build.cake**
  * C# script that tells cake how to build your application

In the **build.cake** file, there is a collection of Tasks that are executed in order by using the **IsDependentOn()** call.

## Running the build script

1. Open a powershell terminal
2. Navigate to the folder you extracted the sample project zip to
3. Run `.\build.ps1`

You might get an error like this:
```
PS C:\Users\Administrator\Downloads\example-master> .\build.ps1
.\build.ps1 : File C:\Users\Administrator\Downloads\example-master\build.ps1 cannot be loaded. The file
C:\Users\Administrator\Downloads\example-master\build.ps1 is not digitally signed. You cannot run this script on the
current system. For more information about running scripts and setting execution policy, see about_Execution_Policies
at http://go.microsoft.com/fwlink/?LinkID=135170.
At line:1 char:1
+ .\build.ps1
+ ~~~~~~~~~~~
    + CategoryInfo          : SecurityError: (:) [], PSSecurityException
    + FullyQualifiedErrorId : UnauthorizedAccess
```

If Windows realizes you downloaded the file from the internet it make block execution.  You can get around this by telling Windows that the files did not come from the internet using a powershell command.

If you received the error, follow these steps:

1. Run `Get-ChildItem .\ | Unblock-File`
2. Close the Powershell Window
3. Reopen the Powershell Window and navigate to the folder
4. Run `.\build.ps1`

```
Test Run Summary
  Overall result: Passed
  Test Count: 2, Passed: 2, Failed: 0, Inconclusive: 0, Skipped: 0
  Start time: 2018-04-22 23:57:57Z
    End time: 2018-04-22 23:57:57Z
    Duration: 0.113 seconds


========================================
Default
========================================

Task                          Duration
--------------------------------------------------
Clean                         00:00:00.0121812
Restore-NuGet-Packages        00:00:02.5454776
Build                         00:00:02.8819069
Run-Unit-Tests                00:00:01.0125675
--------------------------------------------------
Total:                        00:00:06.4554858
```

If you saw the above result, congratulations!  You compiled a C# application and ran a set of unit tests via the command line without installing Visual Studio.

## Try it with your own solution

You can take the **build.cake** file and **build.ps1** file and copy it into your own .NET application's folder.

Then you can edit these lines in the build.ps1 file to point to your own project:

1. `var buildDir = Directory("./src/Example/bin") + Directory(configuration);`
2. `NuGetRestore("./src/Example.sln");`
3. `MSBuild("./src/Example.sln", settings =>`

For your initial try, I would also comment out or remove the **Run-Unit-Tests** task and remove the `.IsDependentOn("Run-Unit-Tests");` section.

Did you have any luck?  If you ran into issues, check out the [Cake website](https://cakebuild.net/) for more information.
