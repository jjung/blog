---
title: "Scoop: a homebrew for Windows" 
date: 2018-08-11
description: "Install apps like vim, grep, and curl"
aliases: 
  - "/posts/installing-windows-cli-apps-with-scoop/"
---

MacOS developers use a tool called [homebrew](https://brew.sh) to install CLI applications.

Windows has a similar project called [Scoop](https://scoop.sh/) to easily install apps like **vim**, **grep**, and **curl**.  Here's some steps to get started.

## Allow PowerShell scripts to be run

First, we need to allow PowerShell scripts to be run in Windows since Scoop uses it.  By default, the PowerShell execution policy is set to **Restricted** which means PowerShell scripts cannot be run at all.

1. Open the PowerShell application
2. `Set-ExecutionPolicy RemoteSigned -scope CurrentUser`
3. Select Yes when asked to change the execution policy

```
Execution Policy Change
The execution policy helps protect you from scripts that you do not trust. Changing the execution policy might expose
you to the security risks described in the about_Execution_Policies help topic at
https:/go.microsoft.com/fwlink/?LinkID=135170. Do you want to change the execution policy?
[Y] Yes  [A] Yes to All  [N] No  [L] No to All  [S] Suspend  [?] Help (default is "N"): Y
```

This changes your execution policy to **RemoteSigned** which allows scripts signed by a trusted publisher to be run.

It also allows scripts that are piped into the PowerShell standard input to be run even if they aren't signed.  This has some security implications, but you'll need to allow this to run any PowerShell scripts.

You can learn more about execution policies [here](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_execution_policies?view=powershell-5.1).

## Install Scoop
`iex (new-object net.webclient).downloadstring('https://get.scoop.sh')`

```
Initializing...
Downloading...
Extracting...
Creating shim...
Adding ~\scoop\shims to your path.
Scoop was installed successfully!
Type 'scoop help' for instructions.
```

This command downloads the PowerShell installation script and then passes the contents of the script to **iex** which is shorthand for **Invoke-Expression**.

Passing the contents to **Invoke-Expression** bypasses the signing check and runs the content of the script without leaving the installation files on your computer.  This technique is similar to entry #8 on this [list](https://blog.netspi.com/15-ways-to-bypass-the-powershell-execution-policy/). 

If you're curious about the contents of the script, you can open up a web browser and go to the [https://get.scoop.sh](https://get.scoop.sh) URL to see the contents.

## Installing applications in scoop

Now you're ready to open up the terminal and start installing apps.\
To install curl: `scoop install curl`

```
Installing '7zip' (18.05) [64bit]
Loading 7z1805-x64.msi from cache
Checking hash of 7z1805-x64.msi ... ok.
Extracting 7z1805-x64.msi ... done.
Linking ~\scoop\apps\7zip\current => ~\scoop\apps\7zip\18.05
Creating shim for '7z'.
Creating shortcut for 7-Zip (7zFM.exe)
'7zip' (18.05) was installed successfully!
Installing 'curl' (7.61.0) [64bit]
curl-7.61.0-win64-mingw.tar.xz (1.9 MB) [=====================================================================] 100%
Checking hash of curl-7.61.0-win64-mingw.tar.xz ... ok.
Extracting curl-7.61.0-win64-mingw.tar.xz ... done.
Linking ~\scoop\apps\curl\current => ~\scoop\apps\curl\7.61.0
Creating shim for 'curl'.
'curl' (7.61.0) was installed successfully!
'curl' suggests installing 'cacert'.
```

When you install an application, Scoop does three things:

1. Installs application into your home folder **(C:\Users\USERNAME\scoop\apps)**.  
2. Uses a shims folder **(C:\Users\USERNAME\scoop\shims)** that links to all the applications in the apps folder.  
3. Adds the location of the shims folder to your system's PATH.  This allows applications installed by Scoop to run in the terminal without typing in the full path.

## Other commands

What else can Scoop do? You can update, uninstall, and search for applications using Scoop.

`scoop help` to get the full list.  

```
Usage: scoop <command> [<args>]

Some useful commands are:

alias       Manage scoop aliases
bucket      Manage Scoop buckets
cache       Show or clear the download cache
checkup     Check for potential problems
cleanup     Cleanup apps by removing old versions
config      Get or set configuration values
create      Create a custom app manifest
depends     List dependencies for an app
export      Exports (an importable) list of installed apps
help        Show help for a command
home        Opens the app homepage
info        Display information about an app
install     Install apps
list        List installed apps
prefix      Returns the path to the specified app
reset       Reset an app to resolve conflicts
search      Search available apps
status      Show status and check for new app versions
uninstall   Uninstall an app
update      Update apps, or Scoop itself
virustotal  Look for app's hash on virustotal.com
which       Locate a shim/executable (similar to 'which' on Linux)
```

## Things to watch out for

After installing or updating an app look at the output.  Sometimes it will recommend installing other dependencies, give you some additional information about the application, or even show that a problem occurred.

For example, when you install **vim**, it suggests that you install **vimtutor**.  Vimtutor is a tutorial for vim but is not required for vim to operate.

Here's a message I received while updating an application.
```
'itcode"' is not recognized as an internal or external command,
operable program or batch file.
```

There's a [github issue](https://github.com/lukesampson/scoop/issues/2384) about this problem.  In it, some people seem to suggest running the scoop update command in powershell instead of cmd.

However, in my case, the application still worked normally despite having that message during update.

## Wrap up

With Scoop, it's easy to install CLI tools like grep or development environments like Node on Windows.  Give it a try the next time you're missing a tool that's available on Linux or Mac.  