---
title: "Post ideas"
date: 2020-07-03
draft: true
---

### Figuring out error messages

Here's a good checklist for figuring out error messages:
- Google
- Stack Overflow
- GitHub issues search
- Search library source code

GitHub issues are underrated. I often find errors there that don't appear on Google. Take time to carefully read issue threads as the context will help you determine if the issue applies to you.

### Asking for help in an e-mail

When writing an e-mail (or DM) keep your message short, specific, and relevant to the person's interest. Make it clear you did your research and you have a specific goal in mind. Don't ask questions the recipient has been asked a hundred times or that is public knowledge.

People skim emails, so consider including three things:
- A title that will be interesting to the recipient
- Why you chose them
- Your specific request

If they don't respond don't take it personally. They could be busy, may have never seen your message, or simply aren't interested.

Some folks openly encourage you to contact them on their webpages or twitter bios. Patrick McKenzie for example has a [standing invitation](https://www.kalzumeus.com/standing-invitation/) and advice on how to approach contacting busy people. 

### Use examples to learn

I wanted to build a theme for the [Hugo](https://www.gohugo.io) static site generator but it was difficult for me to follow the documentation. 

Instead, I viewed the source code for [themes others had made](https://themes.gohugo.io/). By trying and modifying their themes, I was able to gradually understand how Hugo themes worked and use the documentation to fill in gaps.

### Do your research

Learn what you can reading articles and documentation. When an author takes the time to curate additional links look through those as well.

After you're done researching on your own, write specific questions that you couldn't answer. Then reach out to authorities on the subject like users of the technology, open source maintainers, and academics.

> I wrote an article in Smashing Magazine about remote part time teams using agile . . . I did a quick email interview with one of my professors at the time as well as a different professor who had written the book that we were using as the textbook . . . Bringing in those outside perspectives allowed me to write an article that was so much better than anything I could come up with from my own experience. . . . when you're looking for things to use in your own writing look broadly and don't just look at the first page of Google results.

### Don't sell, teach

> And this goes back to what we were quoting earlier from Cassidy Williams . . . We don't like to be sold to but we love to be taught interesting things. And that's why . . . really good content marketing does so well in the developer space.

When we're taught something interesting our natural instinct is to want to learn more. 

Not a perfect analogy but think of free food samples at a grocery store. If you like the free cup of salsa and chips you might buy some. It's a form of marketing that doesn't feel bad.

[Adam Wathan](https://adamwathan.me/) has fantastic free documentation and screencasts for [Tailwind CSS](https://tailwindcss.com/). They helped me understand why the framework exists, how to use it, and even taught me some CSS.

To promote his paid component library [Tailwind UI](https://tailwindui.com/) he made a screencast and provided free sample components to demonstrate their value. 

Provide developers information and let them decide for themselve if they want what you're selling.