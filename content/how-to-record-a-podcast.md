---
title: "How to record a podcast"
date: 2020-07-19
draft: false
description: "Equipment and interview tips"
aliases: 
  - "/posts/how-to-record-a-podcast/"
---

## Introduction

In this article I'll cover some of the equipment and techniques I use to record podcasts.

My background is technical interviews ([Software Sessions](https://www.softwaresessions.com) and [Software Engineering Radio](https://www.se-radio.net)) but most of this applies to any podcast.

Note that you don't need to do anything in this article to get started. You can record using a smart phone and not worry about gear. But if you gave it a try, had fun, and you'd like to improve then I hope you'll keep reading!

## Wear wired headphones

You and your guest should wear headphones or earbuds while recording.

You've probably heard how people's voices get cut off or garbled when people talk at the same time on video calls. This is due to a feature called echo cancellation. You can avoid this and improve recording quality by wearing headphones.

Why wired headphones? Here's some common problems with bluetooth headsets: 

- Trouble pairing headset
- Battery runs out
- Recording software chooses wrong microphone
- Audio gets out of sync with video

If that's all you have then go ahead and use them.

## Choosing a microphone

Starting with earbuds or a laptop microphone is fine. But you can sound professional by spending ~$100.

I recommend these dynamic USB microphones:

- [Samson Q2U](http://www.samsontech.com/samson/products/microphones/usb-microphones/q2u/) (~$60)
- [Audio Technica ATR-2100x](https://www.audio-technica.com/cms/wired_mics/55eee3e0b0d5a960/index.html) (~$100 with USB-C)

If you get the Samson Q2U [update its firmware](https://samsontech.zendesk.com/hc/en-us/articles/360045374974-Q2U-Firmware-). It fixes a bug where it won't work if it's plugged in before your computer is powered on.

You can listen to this [conversation with Ben Orenstein](https://www.softwaresessions.com/episodes/a-decade-long-retrospective-with-ben-orenstein/) to hear what these mics sound like. I'm using the Q2U and Ben has an ATR-2100.

These dynamic microphones sound great in most environments. However you must speak within ~2 inches of the microphone. You'll need to hold the mic, move close to the stand, or use a boom arm.

I recommend purchasing a boom arm and a [shock mount](https://www.amazon.com/Knox-Microphone-Audio-Technica-ATR2100-USB-Samson/dp/B07KBWHJB5/). These help you get the mic close to you without obstructing your view. Here's two boom arms:

- [Samson MBA38](http://www.samsontech.com/samson/products/accessories/microphone-stands/mba38/) 
- [RODE PSA-1](http://www.rode.com/accessories/psa1)

You might wonder why I haven't mentioned the commonly recommended [Blue Yeti](https://www.bluemic.com/en-us/products/yeti/) or [Yeti Nano](https://www.bluemic.com/en-us/products/yeti-nano/). Some like these because you can use them without getting a boom arm or holding them close. Many people put these on their desks. 

The downside is they are very sensitive to outside noise and audio quality can vary greatly based on your room. You can get great sound out of them but it might take some extra work.

If you get one and aren't happy with the sound look up articles on how to reduce echo. I've got some links at the bottom of the page to help with this.

## Recording with a guest online

Use a podcast recording service. I use [Squadcast](https://squadcast.fm/) (~$9 a month) because it provides video chat. I've also used [Zencastr](https://zencastr.com/) (free) which will be adding video soon.

While you talk with your guest their local audio will be uploaded. This means voices won't cut out or sound robotic in the final recording like they do with a zoom call. It also creates separate audio tracks so you can edit out unwanted noise.

As a backup you and your guest should make local recordings. Use Quicktime for MacOS users and [Audacity](https://www.audacityteam.org/) for Windows and Linux users.

Also, **don't forget to press the record button**. People lose shows when they forget this.

## Recording with a guest in-person

1. [Two USB microphones with a Mac](https://www.macworld.com/article/2089460/a-tale-of-two-microphones.html)
 
This is the cheapest option and requires two different microphone models. If you plug in two identical microphones the computer will only recognize one. I have not tried this with a Windows PC.

2. USB interface
 
Many podcasters recommend getting a USB interface instead of plugging multiple microphones directly into your computer. I've heard the [MOTU M2](https://motu.com/en-us/products/m-series/m2/) (~$170) is a good option.

3. Portable recorder

If you record at events you might prefer a portable recorder with a built-in microphone. They are standalone devices that you can also connect your own microphones to.

- [Zoom H4n Pro](https://www.zoom-na.com/products/field-video-recording/field-recording/h4n-pro-handy-recorder) (~$200 for 2 microphones)
- [Zoom H6](https://www.zoom-na.com/products/field-video-recording/field-recording/zoom-h6-handy-recorder-1) (~$350 for 4 microphones).

No matter which option you choose you should record the conversation with your smartphone as a backup. The quality won't be great but at least you'll have something if your equipment fails.

## Preparing for an interview

If you're interviewing someone you should prepare. Your guest is taking time out of their day to chat with you and you should respect that. Many professional podcasts have producers to do research and come up with questions.

Here's a few suggestions:
- Choose some high level topics
- Research your guest's background (blogs, twitter, conference talks, linkedin)
- Identify what's unique about your guest
- Think about what you and your audience would want to know 
- Find past interviews and try to ask things they haven't been asked

Use your questions and high level topics to create an outline for the interview. This gives you a fallback if the conversation stalls.

Give your guest an idea of what you want to talk about. I provide a few sample questions. This gives them a chance to provide feedback or propose different topics.

## Conducting an interview

Have a copy of your outline ready while you talk to your guest. I recommend printing it so you can write notes on it.

Let your guest know the episode will be edited. Tell them they are free to take breaks. If they make a mistake or get stuck invite them to restate their thoughts.

Check if they have a hard stop time and respect it. Skip questions if you are running out of time.

Unscripted conversation will rarely follow your outline exactly. Your guest may answer multiple questions with a single answer. They could go on tangents. You may even think of more interesting questions based on your conversation.

- Listen to the guest's answers and ask good follow up questions
- Decide if the tangent is interesting for your audience or not
- Cross questions off your outline as your guest answers them
- If an answer confuses you ask for clarification
- If you start getting bored move on to a different topic

## Cleaning audio

If you don't have professional audio engineering experience I recommend using [Auphonic](https://www.auphonic.com). There is a web version and a desktop version available. 

Auphonic can:

- Reduce background noise (hum or hiss)
- Level the audio so you and your guest are the same volume
- Make voices sound clearer

It makes a big difference and you can [listen to some examples here](https://auphonic.com/audio_examples).

If you only have a single track to upload, then choose "WAV 16-bit PCM" as your output and check "Mono".

If you have upload separate audio tracks then in the *Output Files* section choose "Individual Tracks" as the format and "wav.zip" as the ending. This will give you two WAV files you can use while editing.

## Editing audio

Respect your listener's time and edit your recordings.

Here's a few examples of things to remove: 

- Pre-interview (troubleshooting, explaining things to the guest)
- Breaks/Interruptions (getting a drink, phone calls)
- Long periods of silence
- Filler words like umms (only if they become distracting)
- Times where the guest makes a mistake and corrects themselves

I recommend using a tool called [Descript](https://www.descript.com) to edit. You upload your audio tracks and it creates a transcript with speaker labels. You can then [edit your audio using text as shown in this video](https://www.youtube.com/watch?v=km3xZvbfWsY). 

Here's some other things it can do: 
- Detect filler words
- Shorten periods of silence
- Create embeddable video clips

## Publishing

[Anchor](https://www.anchor.fm) is a free option to host your podcast that's great for hobbyists. [Submit your podcast to Apple and Spotify yourself](https://help.anchor.fm/hc/en-us/articles/360030793032-Self-submitting-your-podcast-for-distribution) to maintain control over your show. 

The Apple submission process can take a couple weeks. After it is complete your show will appear in most podcast players automatically.

If you don't like Anchor here's some other hosts you can check out:

- [Transistor](https://transistor.fm/) ($19 a month)
- [Pinecast](https://www.pinecast.com/) ($10 a month)
- [Simplecast](https://simplecast.com/) ($14 a month)
- [Libsyn](https://www.libsyn.com/) ($15 a month)

## Anything else?

To reduce echo or improve your mic technique these videos can help:

- [Free Ways to Reduce Echo for Better Sound Quality and Recording](https://www.youtube.com/watch?v=xgL51PXBd1A)
- [10 Tips for Better Sounding Vocal Recordings for Beginners](https://www.youtube.com/watch?v=Ty8YLqOmbV4)
- [Budget Audio Treatment & Budget Vocal Booth](https://www.youtube.com/watch?v=7h84lrQQTl8)

If you want to research more microphones:

- [Best Microphones to Start Podcasting](https://podcastage.com/rev/bestpodcastmics)
- [Podcasting Microphones Mega-Review](https://marco.org/podcasting-microphones)

Have any questions or suggestions? Send me an [e-mail 📧](mailto:contact@softwaresessions.com). Good luck with your podcast!
