---
title: "Writing software development tutorials"
date: 2020-06-03
draft: true
---

Philip Kiely is the author of [Writing for Software Developers](https://philipkiely.com/wfsd/). He's written professionally for clients like [Smashing Magazine](https://www.smashingmagazine.com/) and [CSS Tricks](https://css-tricks.com/). 

Here's a few tips from our conversation.

{{< pinecast "https://pinecast.com/player/482f1812-7d7b-4188-9b9c-8218ce8084a7?theme=flat" >}}

## Pick a clear title

Which makes more sense to you?
- ❌ The Technical Content Development Handbook
- ✅ Writing for Software Developers

The first is Philip's original title for his book. It isn't immediately obvious who the book is for. The second title makes it clear the audience is Software Developers who want to write better.

The second was also picked by Philip's mom 👩 (a professional editor).

## Make it skimmable

Developers in Stack Overflow mode will scroll past your text until they see a code sample. Try to make them runnable and understandable without the context of the article around them. This gives the reader their code snippet and lets them dive deeper if needed. 

Mark McGranaghan's [Go by Example](https://gobyexample.com/) and Stripe's [Integration Builder](https://stripe.com/docs/payments/integration-builder) are great examples of this.

Here's a few other ways to improve skimmability:
- Paragraph breaks
- Bulleted lists
- Diagrams
- Quick summaries

## Pick an audience

Julia Evans has a great tip: [Write for one person](https://twitter.com/b0rk/status/1262415197345636353/photo/1). Her comic explains this better than I can. 

If you don't, your post will be too difficult for beginners and frustrating to read for experts.

## Narrow your scope

Split long posts into shorter ones that remain useful. 

For example, instead of writing one post about Indie Hackers I could split it up into: 
- How they made their site stand out
- Storing data and search (firebase + algolia)
- Caching strategies
- Picking features to work on

Here's a few benefits:
- You're more likely to finish
- Readers can read the part they're interested in
- You can still link them together

## Consider writing professionally

It's usually difficult to get paid to write. Not so with software development tutorials.

Philip wrote for professional publications while he was still in college. Here's a few reasons you might want to try it:

- You'll work with an editor who can help you write better
- It'll get you exposed to a broader audience
- For all my fellow procrastinators you'll get a deadline
- 💰 (of course)

Check out [Who Pays Technical Writers](https://whopaystechnicalwriters.com/) for a list of publishers and how much they pay per article.

## The end

If you'd like to learn more, you can read a transcript of my conversation with Philip [here](https://www.softwaresessions.com/episodes/writing-for-software-developers/).