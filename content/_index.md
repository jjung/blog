---
title: "Jertype"
---

Hi! I'm Jeremy Jung.

ヽ(~_~(・_・ )ゝ

I host the [Software Sessions](https://www.softwaresessions.com) podcast and I hope you're having a good day.

You can reach me via [email](mailto:contact@softwaresessions.com), [bluesky](https://bsky.app/profile/jeremyjung.com), or [mastodon](https://notacult.social/@guu).